import Vue from 'vue'
import Router from 'vue-router'
import CreateAccount from './views/CreateAccount.vue'
import Connexion from './views/Connexion.vue'
import Channels from './views/Channels.vue'
import Members from './views/Members.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/createAccount',
      name: 'createAccount',
      component: CreateAccount
    },
    {
      path: '/connexion',
      name: 'connexion',
      component: Connexion
    },
    {
      path: '/',
      name: 'channels',
      component: Channels
    },
    {
      path: '/members',
      name: 'members',
      component: Members
    }
  ]
})
