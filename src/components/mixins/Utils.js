export const Utils = {
	methods: {
		memberIsConnected() {
			if (this.$store.state.member == false) {
				return false;
			} else {
				this.setTokenAxios(this.$store.state.token);
				return true;
			}
		},

		setTokenAxios(token) {
			window.axios.defaults.params.token = token;
		},

		//used in Connexion and CreateAccount
		signin(email, password) {

			window.axios.post('members/signin', {
				"email": email,
				"password": password

			}).then((response) => {

				this.$store.commit('setMember', {
					fullname : response.data.fullname,
					email : response.data.email,
					id : response.data._id,
				});
				this.$store.commit('setToken', response.data.token);


				this.setAllMemebersInStorage();

				this.$router.push("/");

			}).catch((error) => {
				alert(error.response.data.error);

			});
		},

		setAllMemebersInStorage() {

			window.axios.get('members').then((response) => {

				this.$store.commit('setAllMembers', [
					response.data
				]);

			}).catch((error) => {
				alert(error.message);

			});

		}

	}
}