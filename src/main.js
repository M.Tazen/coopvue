import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'




import axios from 'axios';

import {Utils} from './components/mixins/Utils.js'
Vue.mixin(Utils)

import VModal from 'vue-js-modal'
Vue.use(VModal, { dialog: true})

import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faCoffee)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false


window.axios = axios.create({
  baseURL: 'http://coop.api.netlor.fr/api',
  params : {
    token : false
  },
  headers: {
    Authorization: 'Token token=af18d05b4fb842b39db01b7f997a0bbc'
  }
});

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state));
});



new Vue({
  router,
  store,
  render: h => h(App),
  beforeCreate() {
    this.$store.commit('initialiseStore');
  },
}).$mount('#app')








//api key : c58488c2eedc48c59f650d75019a64ef

// new api key : 26788654ef4d494fa74e63e4d301dfbd
